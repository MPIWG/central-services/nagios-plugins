# MPIWG Nagios custom Plugins

## check_os_version

Returns Linux operating system type and version. Uses the `/usr/bin/lsb_release` command.
Currently only works with Ubuntu release versions.

Command to show OS and version
```
check_os_version
```
Output
```
OS - Ubuntu 18.04
```
Command to test version (warn if older than 2 years)
```
check_os_version -w 2
```
Output
```
OS WARNING - Ubuntu 18.04 older than 2 years
```

## Set up nagios-plugins on remote server

Install Nagios NRPE server
```
apt install nagios-nrpe-server
```
Check out MPIWG nagios-plugins in e.g. `/var/tmp/src`
```
cd /var/tmp/src
git clone https://gitlab.gwdg.de/MPIWG/central-services/nagios-plugins.git
```
Install scripts in e.g. `/usr/local/sbin`
```
cp nagios-plugins/check_os_version /usr/local/sbin/
chmod a+x /usr/local/sbin/check_os_version
```
Add plugin script to NRPE config in `/etc/nagios/nrpe.cfg` or better `/etc/nagios/nrpe_local.cfg`:
```
command[check_os_version]=/usr/local/sbin/check_os_version -w 4 -c 5
```
